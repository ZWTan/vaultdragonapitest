console.log('Loading function');

var AWS = require('aws-sdk');
var moment = require('moment');
var uuid = require('uuid');
var dynamo = new AWS.DynamoDB();

var dynamoDBTable  = "tblObjects";

// Ref for VaultDragon Test Case: https://gist.github.com/rahij/3abfb7d5f546a969e9679a73bf3bc85f

exports.handler = function(event, context, callback) {

    console.log("Inside handler now");
    
    console.log("http_method is: " + event.httpmethod);
    console.log("body is: " + JSON.stringify(event.body));
    console.log("DynamoDB Table name: " + dynamoDBTable);
    console.log("querystring: " + event.querystring);


    var httpMethod = event.httpmethod;

    switch(httpMethod) {
        case 'POST':
                // Use this to exit from queue and pending status to false = done
            console.log("API Gateway: POST");
            createObject();
            break;
        case 'GET':
            console.log("API Gateway: GET");
            getObject();
            break;
        default:
            console.log("DEFAULT HIT!");
    }   // End Switch(httpMethod)
    
    function createObject()
    {
        console.log("Inside createObject function");

        var errorMessage = "Missing property ";
        var errorOccur = false;
        //JSON Body: {mykey : value1}
        if(!event.body.Item.hasOwnProperty('mykey'))
        {
            console.log("Missing property 'mykey'");
            errorMessage += " 'mykey'";
            errorOccur = true;
        }
        if(!event.body.Item.hasOwnProperty('content'))
        {
            console.log("Missing property 'content'");
            errorMessage += " 'content'";
            errorOccur = true;
        }

        if(errorOccur === true)
        {
            context.fail(errorMessage);
            return;
        }

        var UUID = uuid.v1();

        console.log("Unix time: " + moment().unix());
        console.log("Timestamp: " + moment().utcOffset(8).format('YYYY-MM-DD HH:mm'));

        dynamo.putItem({
            "TableName": dynamoDBTable,
            "Item": {
                //"object_id": {"S": UUID},
                "object_mykey": {"S": event.body.Item.mykey },
                "object_jsontime": {"S": moment().unix().toString() },
                "object_timestamp": {"S": moment().utcOffset(8).format('YYYY-MM-DD HH:mm') },
                "object_content": {"S": event.body.Item.content }
                //"object_value": {"S": event.body.Item.mykey }
            }
        }, function(err, data) {
            var message ="";
            if(err)
            {
                message = {'status': 'fail', 'errorMessage': JSON.stringify(err)};
                console.log(message);
                context.fail(message);
            }
            else
            {
                message = {'status': 'success', 'mykey': event.body.Item.mykey, 'Timestamp': moment().utcOffset(8).format('YYYY-MM-DD HH:mm'), 'Unix':  moment().unix() };
                console.log(message);
                context.succeed(message);
            }
        });
    }   // End createObject()



    function getObject()
    {

        console.log("Inside getObject function");
        console.log("objectid: " + event.objectid);
        // Check if querystring exist before scanning DynamoDB
        var params = {};
        expr = /timestamp/;
        // Search with UNIX timestamp given
        if(event.querystring.match(expr))
        {
            console.log("There timestamp in querystring!");
            console.log("UNIX Value: " + getValuePart(event.querystring).toString());
            params = {
                TableName: dynamoDBTable,
                /*KeyConditions: {
                    object_mykey: { S: event.objectid.toString() },
                    object_jsontime: { S: getValuePart(event.querystring).toString() }
                },*/
                KeyConditions: { 
                    object_mykey: { ComparisonOperator: 'EQ', AttributeValueList: [ {S: event.objectid.toString()} ] }
                    //object_jsontime: { ComparisonOperator: 'EQ', AttributeValueList : [ { S: "1490074686"} ] }
                },
            };  

            scanDynamoItem(params);
        }
        else    // Search normally without UNIX timestamp
        {
            console.log("No timestamp in querystring.");
            params = {
                TableName: dynamoDBTable,
                Key: {
                    object_mykey: { S: event.objectid.toString() }
                }
            };

            getDynamoItem(params);
        }

        console.log("Checking PARAM: " + JSON.stringify(params));
    }   // End getObject()

    function scanDynamoItem(params)
    {
        console.log("Inside scanDynamoItem Func");

        dynamo.query(params, function(err, data) {
            if (err)
            {
                console.log(err); // an error occurred
                message = {'status': 'fail', 'errorMessage': 'No such record found'};
                console.log(message);
                context.fail(JSON.stringify(message));

            } 
            else
            {
                console.log(data); // successful response
                console.log("UNIX TIME: " + data.Items[0].object_jsontime.S);

                if(data.Items[0].object_jsontime.S === getValuePart(event.querystring).toString())
                {
                    console.log("Value matched");
                    message = {'status': 'success', 'mykey': data.Items[0].object_mykey.S, 'Timestamp': data.Items[0].object_timestamp.S, 'Content':  data.Items[0].object_content.S };
                    context.succeed(message);
                }
                else
                {
                    console.log("Value mismatch");
                    message = {'status': 'fail', 'errorMessage': 'No such record found'};
                    context.fail(JSON.stringify(message));
                }

                
            }
        })

    }   // End scanDynamoItem()

    function getDynamoItem(params)
    {
        console.log("Inside getDynamoItem Func");
        dynamo.getItem(params, function(err, data) {
            try
            {
                // successful response
                console.log(data);
                //console.log(data.Item.object_mykey);
                //console.log(data.Item.object_timestamp);

                message = {'status': 'success', 'mykey': data.Item.object_mykey.S, 'Timestamp': data.Item.object_timestamp.S, 'Unix':  data.Item.object_jsontime.S };
                console.log(message);
                context.succeed(message);
            }
            catch(err)
            {
                // an error occurred
                console.log(err);
                message = {'status': 'fail', 'errorMessage': 'No ID found'};
                console.log(message);
                context.fail(JSON.stringify(message));
            }
        });
    }   // End getDynamoItem()

    function getValuePart(str) {
        console.log("Inside getValuePart Func");
        // Use to split querystring after =
        var newStr = str.split('=')[1];
        return newStr.slice(0, -1);
    }   // End getValuePart()

};  